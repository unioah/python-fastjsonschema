%global _empty_manifest_terminate_build 0
Name:		python-fastjsonschema
Version:	2.15.3
Release:	1
Summary:	Fastest Python implementation of JSON schema
License:	BSD
URL:		https://github.com/seznam/python-fastjsonschema
Source0:	fastjsonschema-2.15.3.tar.gz
BuildArch:	noarch


%description


%package -n python3-fastjsonschema
Summary:	Fastest Python implementation of JSON schema
Provides:	python-fastjsonschema
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-fastjsonschema


%package help
Summary:	Development documents and examples for fastjsonschema
Provides:	python3-fastjsonschema-doc
%description help


%prep
%autosetup -n fastjsonschema-2.15.3

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-fastjsonschema -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sat Feb 26 2022 Python_Bot <Python_Bot@openeuler.org> - 2.15.3-1
- Package Spec generated
